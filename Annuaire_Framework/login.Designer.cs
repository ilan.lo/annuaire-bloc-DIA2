﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Annuaire_Framework.Models;

namespace Annuaire_Framework
{
    partial class login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(login));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.textBox1.Location = new System.Drawing.Point(153, 171);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '*';
            this.textBox1.Size = new System.Drawing.Size(333, 26);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label1.Location = new System.Drawing.Point(153, 117);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "Entrez Mot De Passe";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(108)))), ((int)(((byte)(48)))));
            this.button1.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.button1.Location = new System.Drawing.Point(398, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "Entrer";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.mdp_Click);
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(196)))), ((int)(((byte)(235)))));
            this.ClientSize = new System.Drawing.Size(529, 335);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "login";
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private void mdp_Click(object sender, EventArgs e)
        {
            Console.WriteLine(this.textBox1.Text);
            Admin admin = Controllers.APIMethods.GetAdmin();
            // Encodage du mot de passe saisie
            string mdp = this.textBox1.Text;
            string encodePassword;
            try
            {
                byte[] encData_byte = new byte[mdp.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(mdp);
                encodePassword = Convert.ToBase64String(encData_byte);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue. Veuillez réessayer ulterieurment.");
                return;
            }

            // Vérifie si le mot de passe saisie est valide ou invalide
            if(encodePassword != admin.mdp)
            {
                MessageBox.Show("Mot de passe inccorecte");
                return;
            }
            else
            {
                //Form1 form1 = new Form1();
                //form1.Show();
                this.Close();
                Annuaire_Framework.Form1.adm = 1;
                Annuaire_Framework.Form1.print();
                //chargement de la fonction pour afficher les truc invisible
                //ie bouton modifier - supprimer
                
            }
        }

        private System.Windows.Forms.Button button1;

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;

        #endregion
    }
}
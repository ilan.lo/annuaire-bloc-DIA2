﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Annuaire_Framework.Controllers;
using Annuaire_Framework.Models;

namespace Annuaire_Framework
{
    public partial class AdminData : Form
    {
        public AdminData()
        {
            InitializeComponent();
            fillData();
        }

        private void fillData()
        {
            List<Service> services = Controllers.APIMethods.GetAllService();
            List<Site> sites = Controllers.APIMethods.GetAllSite();
            int i = 0;
            
            dataGridView1.Rows.Clear(); //site
            dataGridView2.Rows.Clear(); //service
            foreach (var service in services)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].Cells[0].Value = service.ser_id;
                dataGridView1.Rows[i].Cells[1].Value = service.ser_nom;
                i++;
            }
            i = 0;
            foreach (var site in sites)
            {
                dataGridView2.Rows.Add();
                dataGridView2.Rows[i].Cells[0].Value = site.sit_id;
                dataGridView2.Rows[i].Cells[1].Value = site.sit_ville;
                i++;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //modifier service
            DataGridView tab = dataGridView1;
            int index = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[index].Cells[0].Value.ToString() == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de service");
                return;
            }
            Service service = new Service();
            service.ser_id = Int32.Parse(tab.Rows[index].Cells[0].Value.ToString());
            service.ser_nom = textBox1.Text;
            Debug.WriteLine(service.ser_id);
            Debug.WriteLine(service.ser_nom);
            if (service.ser_nom == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de service");
                return;
            }
            try
            {
                APIMethods.UpdateService(service);
            }
            catch (Exception)
            {
            }
            fillData();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //modifier ville
            DataGridView tab = dataGridView2;
            int index = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[index].Cells[0].Value.ToString() == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de site");
                return;
            }
            Site site = new Site();
            site.sit_id = Int32.Parse(tab.Rows[index].Cells[0].Value.ToString());
            site.sit_ville = textBox2.Text;
            Debug.WriteLine(site.sit_id);
            Debug.WriteLine(site.sit_ville);
            if (site.sit_ville == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de site");
                return;
            }
            try
            {
                APIMethods.UpdateSite(site);
            }
            catch (Exception)
            {
            }
            fillData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //supprimer service
            DataGridView tab = dataGridView1;
            int index = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[index].Cells[0].Value.ToString() == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de service");
                return;
            }
            Service service = new Service();
            service.ser_id = Int32.Parse(tab.Rows[index].Cells[0].Value.ToString());
            Debug.WriteLine(service.ser_id);
            if (service.ser_nom == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de service");
                return;
            }
            try
            {
                APIMethods.DeleteServiceById(service.ser_id);
            }
            catch (Exception)
            {
            }
            fillData();
        }




        private void button3_Click(object sender, EventArgs e)
        {
            //supprimer droite
            DataGridView tab = dataGridView2;
            int index = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[index].Cells[0].Value.ToString() == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de site");
                return;
            }
            Site site = new Site();
            site.sit_id = Int32.Parse(tab.Rows[index].Cells[0].Value.ToString());
            Debug.WriteLine(site.sit_id);
            if (site.sit_ville == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de site");
                return;
            }
            try
            {
                APIMethods.DeleteSiteById(site.sit_id);
            }
            catch (Exception)
            {
            }
            fillData();
        }

        private void clickVille(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView tab = dataGridView1;
            int selectedLine = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[selectedLine].Cells[0].Value.ToString() == "")
            {
                return;
            }

            textBox1.Text = tab.Rows[selectedLine].Cells[1].Value.ToString();
        }
        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            DataGridView tab = dataGridView2;
            int selectedLine = tab.SelectedCells[0].RowIndex;
            if (tab.Rows[selectedLine].Cells[0].Value.ToString() == "")
            {
                return;
            }

            textBox2.Text = tab.Rows[selectedLine].Cells[1].Value.ToString();
        }

    }
}
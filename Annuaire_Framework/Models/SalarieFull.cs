﻿using Newtonsoft.Json;
namespace Annuaire_Framework.Models
{
    public class SalarieFull
    {
        [JsonProperty("sal_id")]
        public int sal_id { get; set; }
    
        [JsonProperty("sal_nom")]
        public string sal_nom { get; set; }
    
        [JsonProperty("sal_prenom")]
        public string sal_prenom { get; set; }
    
        [JsonProperty("sal_fixe")]
        public string sal_fixe { get; set; }
    
        [JsonProperty("sal_port")]
        public string sal_port { get; set; }
    
        [JsonProperty("sal_mail")]
        public string sal_mail { get; set; }
    
        [JsonProperty("ser_id")]
        public int ser_id { get; set; }
    
        [JsonProperty("sit_id")]
        public int sit_id { get; set; }
        
        [JsonProperty("ser_nom")]
        public string ser_nom { get; set; }
    
        [JsonProperty("sit_ville")]
        public string sit_ville { get; set; }
        
    }
}
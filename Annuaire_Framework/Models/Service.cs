﻿using Newtonsoft.Json;
namespace Annuaire_Framework.Models
{
    public class Service
    {
        [JsonProperty("ser_id")]
        public int ser_id { get; set; }
    
        [JsonProperty("ser_nom")]
        public string ser_nom { get; set; }
    }
}
﻿using Newtonsoft.Json;
namespace Annuaire_Framework.Models
{
    public class Site
    {
        [JsonProperty("sit_id")]
        public int sit_id { get; set; }
    
        [JsonProperty("sit_ville")]
        public string sit_ville { get; set; }
    }
}
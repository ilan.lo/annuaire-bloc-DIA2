﻿using System;
using System.Windows.Forms;
using Annuaire_Framework.Controllers;
using Annuaire_Framework.Models;

namespace Annuaire_Framework
{
    public partial class NewSalarie : Form
    {
        public NewSalarie()
        {
            InitializeComponent();
            FillData();
        }

        private void FillData()
        {
            var villes = APIMethods.GetAllSite();
            var services = APIMethods.GetAllService();
            
            this.SendSite.Text = "";
            this.SendSite.Items.Clear();
            this.SendSite.Items.Add("None");
            this.SendService.Text = "";
            this.SendService.Items.Clear();
            this.SendService.Items.Add("None");

            foreach (var site in villes)
            {
                this.SendSite.Items.Add(site.sit_ville);
            }
            foreach (var service in services)
            {
                SendService.Items.Add(service.ser_nom);
            }
        }

        private void NewSalarie_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }

        private void label7_Click(object sender, EventArgs e)
        {
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            //send new salarie
            Salarie salarie = new Salarie();
            salarie.sit_id = SendSite.SelectedIndex;
            salarie.ser_id = SendService.SelectedIndex;
            salarie.sal_nom = SendNom.Text;
            salarie.sal_prenom = SendPrenom.Text;
            salarie.sal_mail = SendMail.Text;
            salarie.sal_fixe = SendFixe.Text;
            salarie.sal_port = SendPort.Text;
            APIMethods.PostSalarie(salarie);
            Form1.fillData();
            this.Close();
        }

        private void newSite_Click(object sender, EventArgs e)
        {
            Site site = new Site();
            site.sit_ville = this.textBox1.Text;
            APIMethods.PostSite(site);
            Form1.fillData();
            this.Close();
        }

        private void newService_Click(object sender, EventArgs e)
        {
            Service service = new Service();
            service.ser_nom = this.textBox2.Text;
            APIMethods.PostService(service);
            Form1.fillData();
            this.Close();
        }

    }
}
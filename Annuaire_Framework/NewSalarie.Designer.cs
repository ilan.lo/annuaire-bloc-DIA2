﻿using System.ComponentModel;

namespace Annuaire_Framework
{
    partial class NewSalarie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>²²²²²²²
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewSalarie));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SendMail = new System.Windows.Forms.TextBox();
            this.SendSite = new System.Windows.Forms.ComboBox();
            this.SendPort = new System.Windows.Forms.TextBox();
            this.SendNom = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SendFixe = new System.Windows.Forms.TextBox();
            this.SendService = new System.Windows.Forms.ComboBox();
            this.SendPrenom = new System.Windows.Forms.TextBox();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.newService = new FontAwesome.Sharp.IconButton();
            this.newSite = new FontAwesome.Sharp.IconButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.iconButton1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(24, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(673, 341);
            this.panel1.TabIndex = 0;
            // 
            // iconButton1
            // 
            this.iconButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(108)))), ((int)(((byte)(48)))));
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.iconButton1.IconColor = System.Drawing.Color.Black;
            this.iconButton1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton1.Location = new System.Drawing.Point(288, 201);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Size = new System.Drawing.Size(123, 65);
            this.iconButton1.TabIndex = 9;
            this.iconButton1.UseVisualStyleBackColor = false;
            this.iconButton1.Click += new System.EventHandler(this.iconButton1_Click);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label7.Location = new System.Drawing.Point(343, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 26);
            this.label7.TabIndex = 8;
            this.label7.Text = "Prenom";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label6.Location = new System.Drawing.Point(337, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 26);
            this.label6.TabIndex = 7;
            this.label6.Text = "Fixe";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label5.Location = new System.Drawing.Point(331, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 26);
            this.label5.TabIndex = 6;
            this.label5.Text = "Service";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label4.Location = new System.Drawing.Point(252, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 26);
            this.label4.TabIndex = 5;
            this.label4.Text = "Mail";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label3.Location = new System.Drawing.Point(244, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Site";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label2.Location = new System.Drawing.Point(244, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Portable";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.label1.Location = new System.Drawing.Point(252, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.SendMail);
            this.panel3.Controls.Add(this.SendSite);
            this.panel3.Controls.Add(this.SendPort);
            this.panel3.Controls.Add(this.SendNom);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(230, 341);
            this.panel3.TabIndex = 1;
            // 
            // SendMail
            // 
            this.SendMail.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendMail.Location = new System.Drawing.Point(43, 152);
            this.SendMail.Name = "SendMail";
            this.SendMail.Size = new System.Drawing.Size(147, 26);
            this.SendMail.TabIndex = 2;
            // 
            // SendSite
            // 
            this.SendSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SendSite.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendSite.FormattingEnabled = true;
            this.SendSite.Location = new System.Drawing.Point(44, 110);
            this.SendSite.Name = "SendSite";
            this.SendSite.Size = new System.Drawing.Size(146, 27);
            this.SendSite.TabIndex = 2;
            // 
            // SendPort
            // 
            this.SendPort.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendPort.Location = new System.Drawing.Point(43, 68);
            this.SendPort.Name = "SendPort";
            this.SendPort.Size = new System.Drawing.Size(147, 26);
            this.SendPort.TabIndex = 1;
            // 
            // SendNom
            // 
            this.SendNom.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendNom.Location = new System.Drawing.Point(43, 27);
            this.SendNom.Name = "SendNom";
            this.SendNom.Size = new System.Drawing.Size(147, 26);
            this.SendNom.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.SendFixe);
            this.panel2.Controls.Add(this.SendService);
            this.panel2.Controls.Add(this.SendPrenom);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(445, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(228, 341);
            this.panel2.TabIndex = 0;
            // 
            // SendFixe
            // 
            this.SendFixe.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendFixe.Location = new System.Drawing.Point(17, 68);
            this.SendFixe.Name = "SendFixe";
            this.SendFixe.Size = new System.Drawing.Size(146, 26);
            this.SendFixe.TabIndex = 1;
            // 
            // SendService
            // 
            this.SendService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SendService.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendService.FormattingEnabled = true;
            this.SendService.Location = new System.Drawing.Point(17, 110);
            this.SendService.Name = "SendService";
            this.SendService.Size = new System.Drawing.Size(146, 27);
            this.SendService.TabIndex = 3;
            // 
            // SendPrenom
            // 
            this.SendPrenom.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.SendPrenom.Location = new System.Drawing.Point(17, 27);
            this.SendPrenom.Name = "SendPrenom";
            this.SendPrenom.Size = new System.Drawing.Size(146, 26);
            this.SendPrenom.TabIndex = 0;
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            this.dataGridTextBoxColumn1.Width = -1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.newService);
            this.panel4.Controls.Add(this.newSite);
            this.panel4.Controls.Add(this.textBox2);
            this.panel4.Controls.Add(this.textBox1);
            this.panel4.Location = new System.Drawing.Point(25, 402);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(672, 164);
            this.panel4.TabIndex = 1;
            // 
            // newService
            // 
            this.newService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(108)))), ((int)(((byte)(48)))));
            this.newService.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.newService.IconChar = FontAwesome.Sharp.IconChar.None;
            this.newService.IconColor = System.Drawing.Color.Black;
            this.newService.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.newService.Location = new System.Drawing.Point(462, 54);
            this.newService.Name = "newService";
            this.newService.Size = new System.Drawing.Size(146, 78);
            this.newService.TabIndex = 11;
            this.newService.Text = "Ajouter un service";
            this.newService.UseVisualStyleBackColor = false;
            this.newService.Click += new System.EventHandler(this.newService_Click);
            // 
            // newSite
            // 
            this.newSite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(108)))), ((int)(((byte)(48)))));
            this.newSite.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.newSite.IconChar = FontAwesome.Sharp.IconChar.None;
            this.newSite.IconColor = System.Drawing.Color.Black;
            this.newSite.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.newSite.Location = new System.Drawing.Point(43, 54);
            this.newSite.Name = "newSite";
            this.newSite.Size = new System.Drawing.Size(146, 78);
            this.newSite.TabIndex = 10;
            this.newSite.Text = "Ajouter un site";
            this.newSite.UseVisualStyleBackColor = false;
            this.newSite.Click += new System.EventHandler(this.newSite_Click);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.textBox2.Location = new System.Drawing.Point(462, 22);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(146, 26);
            this.textBox2.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Berlin Sans FB", 10.2F);
            this.textBox1.Location = new System.Drawing.Point(43, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(146, 26);
            this.textBox1.TabIndex = 2;
            // 
            // NewSalarie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(196)))), ((int)(((byte)(235)))));
            this.ClientSize = new System.Drawing.Size(882, 589);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NewSalarie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NewSalarie";
            this.Load += new System.EventHandler(this.NewSalarie_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
        }

        private FontAwesome.Sharp.IconButton newService;

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private FontAwesome.Sharp.IconButton newSite;

        private FontAwesome.Sharp.IconButton iconButton1;

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;

        private System.Windows.Forms.TextBox SendPort;
        private System.Windows.Forms.ComboBox SendSite;
        private System.Windows.Forms.ComboBox SendService;

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox SendPrenom;
        private System.Windows.Forms.TextBox SendFixe;
        private System.Windows.Forms.TextBox SendMail;
        private System.Windows.Forms.TextBox SendNom;

        private System.Windows.Forms.Panel panel1;

        #endregion
    }
}
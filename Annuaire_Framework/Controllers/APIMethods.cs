﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Annuaire_Framework.Models;

namespace Annuaire_Framework.Controllers
{
    public class APIMethods
    {
        //admin
        public static Admin GetAdmin()
        {
            return Controllers.APIController.GetAllReviews<Admin>("/Admin/1").Result.First();
        }
        public static bool UpdateAdmin(Admin admin)
        {
            return APIController.PutItem<Admin>(admin, "/Admin/put", 1); // .First()?
        }
        //site
        public static List<Site> GetAllSite()
        {
            return Controllers.APIController.GetAllReviews<Site>("/Site").Result;
        }
        public static Site GetSiteById(int id)
        {
            return Controllers.APIController.GetReviewById<Site>("/Site", id).Result.First();
        }
        public static bool UpdateSite(Site site)
        {
            return APIController.PutItem<Site>(site, "/Site/put", site.sit_id);
        }
        public static bool PostSite(Site site)
        {
            return APIController.PostItem<Site>(site, "/Site/post");
        }
        public static bool DeleteSiteById(int id)
        {
            return APIController.DeleteItem("/Site/delete", id);
        }
        //service
        public static List<Service> GetAllService()
        {
            return Controllers.APIController.GetAllReviews<Service>("/Service").Result;
        }
        public static Service GetServiceById(int id)
        {
            return Controllers.APIController.GetReviewById<Service>("/Service", id).Result.First();
        }
        public static bool UpdateService(Service service)
        {
            return APIController.PutItem<Service>(service, "/Service/put", service.ser_id);
        }
        public static bool PostService(Service service)
        {
            return APIController.PostItem<Service>(service, "/Service/post");
        }
        public static bool DeleteServiceById(int id)
        {
            return APIController.DeleteItem("/Service/delete", id);
        }
        //salarie
        public static List<SalarieFull> GetAllSalarie()
        {
            return Controllers.APIController.GetAllReviews<SalarieFull>("/Salarie").Result;
        }
        public static SalarieFull GetSalarieById(int id)
        {
            return Controllers.APIController.GetReviewById<SalarieFull>("/Salarie", id).Result.First();
        }
        public static List<SalarieFull> GetSalarieLike(string search)
        {
            return Controllers.APIController.GetReviewById<SalarieFull>("/Salarie/like", search).Result;
        }
        public static bool UpdateSalarie(Salarie salarie)
        {
            Debug.WriteLine(salarie.sal_id);
            Debug.WriteLine(salarie.ser_id);
            Debug.WriteLine(salarie.sit_id);
            Debug.WriteLine(salarie.sal_nom);
            Debug.WriteLine(salarie.sal_prenom);
            Debug.WriteLine(salarie.sal_mail);
            Debug.WriteLine(salarie.sal_fixe);
            Debug.WriteLine(salarie.sal_port);
            return APIController.PutItem<Salarie>(salarie, "/Salarie/put", salarie.sal_id);
        }
        public static bool PostSalarie(Salarie salarie)
        {
            return APIController.PostItem<Salarie>(salarie, "/Salarie/post");
        }
        public static bool DeleteSalarieById(int id)
        {
            return APIController.DeleteItem("/Salarie/delete", id);
        }
        
        
        
        
    }
}
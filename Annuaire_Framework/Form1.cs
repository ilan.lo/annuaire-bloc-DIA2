﻿using Annuaire_Framework.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Annuaire_Framework.Controllers;

namespace Annuaire_Framework
{
    public partial class Form1 : Form
    {
        public static int adm = 0;
        public static Form1 formData;
        private int space = 0;
        private int a = 0;
        private Form fenetreLog;
        private static Form pageAdd;
        private static Form admData;
        private static DataGridView _grid;
        private static ComboBox _comboSite1;
        private static ComboBox _comboSite2;
        private static ComboBox _comboServ1;
        private static ComboBox _comboServ2;
        public Form1()
        {
            InitializeComponent();
            _grid = this.dataGridView1;
            _comboSite1 = this.comboBox1;
            _comboSite2 = this.comboBoxFicheSite;
            _comboServ1 = this.comboBox2;
            _comboServ2 = this.comboBoxFicheService;
            formData = this;
            fillData();
            //this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(Form1_KeyDown);

            this.fenetreLog = new login();
            this.fenetreLog.Visible = false;
            printAdmin();
        }
        public static void print()
        {
            formData.printAdmin();
        }
        public void printAdmin()
        {
            if (adm == 1)
            {
                iconModify.Visible = true;
                iconButton3.Visible = true;
                iconButton1.Visible = true;
                button2.Visible = true;
                button3.Visible = true;
            }
        }
        void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (adm == 1)
            {
                Console.WriteLine("admin"+adm);
                return;
            }
                
            if (e.KeyCode == Keys.CapsLock)
            {
                Console.WriteLine("clicked");  // This will call the button1_Click event handler
                space = 1;
            }
            if (e.KeyCode == Keys.ControlKey)
            {
                Console.WriteLine("A");
                a = 1;
            }

            if (a == 1 && space == 1)
            {
                Console.WriteLine("c'est fait");
                try
                {
                    this.fenetreLog.Show();
                }
                catch (Exception)
                {
                    this.fenetreLog = new login();
                    this.fenetreLog.Show();
                }
                this.fenetreLog.BringToFront();
            }
        }

        public static void fillData()
        {
            List<SalarieFull> salaries = Controllers.APIMethods.GetAllSalarie();
            int i = 0;
            
            _grid.Rows.Clear();
            foreach (var salarie in salaries)
            {
                _grid.Rows.Add();
                _grid.Rows[i].Cells[0].Value = salarie.sal_nom;
                _grid.Rows[i].Cells[1].Value = salarie.sal_prenom;
                _grid.Rows[i].Cells[2].Value = salarie.sal_fixe;
                _grid.Rows[i].Cells[3].Value = salarie.sal_port;
                _grid.Rows[i].Cells[4].Value = salarie.sal_mail;
                _grid.Rows[i].Cells[5].Value = salarie.ser_nom;
                _grid.Rows[i].Cells[6].Value = salarie.sit_ville;
                _grid.Rows[i].Cells[7].Value = salarie.ser_id;
                _grid.Rows[i].Cells[8].Value = salarie.sit_id;
                _grid.Rows[i].Cells[9].Value = salarie.sal_id;
                i++;
            }

            List<Site> sites = Controllers.APIMethods.GetAllSite();
            List<Service> services = Controllers.APIMethods.GetAllService();
            
            _comboSite1.Text = "";
            _comboSite1.Items.Clear();
            _comboSite1.Items.Add("None");
            _comboServ1.Text = "";
            _comboServ1.Items.Clear();
            _comboServ1.Items.Add("None");
            _comboServ2.Text = "";
            _comboServ2.Items.Clear();
            _comboServ2.Items.Add("None");
            _comboSite2.Text = "";
            _comboSite2.Items.Clear();
            _comboSite2.Items.Add("None");

            foreach (var site in sites)
            {
                _comboSite1.Items.Add(site.sit_ville);
                _comboSite2.Items.Add(site.sit_ville);
            }
            foreach (var service in services)
            {
                _comboServ1.Items.Add(service.ser_nom);
                _comboServ2.Items.Add(service.ser_nom);
            }
        }
        private void fillDataSearch()
        {
            Console.WriteLine("here");
            string search = this.textBox1.Text;
            Console.WriteLine(search);
            List<SalarieFull> salaries;
            if (search == "")
                salaries = Controllers.APIMethods.GetAllSalarie();
            else
                salaries = Controllers.APIMethods.GetSalarieLike(search);
            int i = 0;
            int sitId = -1;
            int serId = -1;
            if (comboBox1.SelectedIndex > 0)
            {
                sitId = comboBox1.SelectedIndex;
            }
            if (comboBox2.SelectedIndex > 0)
            {
                serId = comboBox2.SelectedIndex;
            }
            this.dataGridView1.Rows.Clear();
            foreach (var salarie in salaries)
            {
                if (sitId == -1 || sitId == salarie.sit_id)
                {
                    if (serId == -1 || serId == salarie.ser_id)
                    {
                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[i].Cells[0].Value = salarie.sal_nom;
                        this.dataGridView1.Rows[i].Cells[1].Value = salarie.sal_prenom;
                        this.dataGridView1.Rows[i].Cells[2].Value = salarie.sal_fixe;
                        this.dataGridView1.Rows[i].Cells[3].Value = salarie.sal_port;
                        this.dataGridView1.Rows[i].Cells[4].Value = salarie.sal_mail;
                        this.dataGridView1.Rows[i].Cells[5].Value = salarie.ser_nom;
                        this.dataGridView1.Rows[i].Cells[6].Value = salarie.sit_ville;
                        this.dataGridView1.Rows[i].Cells[7].Value = salarie.ser_id.ToString();
                        this.dataGridView1.Rows[i].Cells[8].Value = salarie.sit_id.ToString();
                        this.dataGridView1.Rows[i].Cells[9].Value = salarie.sal_id.ToString();
                        i++;
                    }
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // Récupération du tableau
                DataGridView tab = dataGridView1;
                // Récupération de la ligne sélectionnée
                int selectedLine = tab.SelectedCells[0].RowIndex;
                if (tab.Rows[selectedLine].Cells[0].Value.ToString() == "")
                {
                    return;
                }

                textBox2.Text = tab.Rows[selectedLine].Cells[0].Value.ToString();
                textBox3.Text = tab.Rows[selectedLine].Cells[1].Value.ToString();
                textBox4.Text = tab.Rows[selectedLine].Cells[2].Value.ToString();
                textBox5.Text = tab.Rows[selectedLine].Cells[3].Value.ToString();
                textBox6.Text = tab.Rows[selectedLine].Cells[4].Value.ToString();
                comboBoxFicheService.Text = tab.Rows[selectedLine].Cells[5].Value.ToString();
                comboBoxFicheSite.Text = tab.Rows[selectedLine].Cells[6].Value.ToString();
                ServiceId.Text = tab.Rows[selectedLine].Cells[7].Value.ToString();
                siteId.Text = tab.Rows[selectedLine].Cells[8].Value.ToString();
                salId.Text = tab.Rows[selectedLine].Cells[9].Value.ToString();
                int i = 0;
                foreach (var choix in comboBoxFicheService.Items)
                {
                    i++;
                    if (comboBox2.Text == choix.ToString())
                    {
                        ServiceId.Text = i.ToString();

                    }
                }

                i = 0;
                foreach (var choix in comboBox2.Items)
                {
                    i++;
                    if (comboBoxFicheSite.Text == choix.ToString())
                    {
                        siteId.Text = i.ToString();

                    }
                }
                return;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception);
            }
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fillDataSearch();
        }

        private void addPpl()
        {
            List<string[]> personnes = new List<string[]>()
            {
                new string[] { "Dupont", "Jean", "jean.dupont@gmail.com", "01 23 45 67 89", "06 12 34 56 78" },
                new string[] { "Martin", "Marie", "marie.martin@hotmail.com", "02 34 56 78 90", "07 23 45 67 89" },
                new string[] { "Laurent", "Pierre", "pierre.laurent@yahoo.com", "03 45 67 89 01", "06 34 56 78 90" },
                new string[] { "Garcia", "Ana", "ana.garcia@gmail.com", "04 56 78 90 12", "07 45 67 89 01" },
                new string[] { "Dubois", "Sophie", "sophie.dubois@hotmail.com", "05 67 89 01 23", "06 56 78 90 12" },
                new string[] { "Martinez", "Carlos", "carlos.martinez@yahoo.com", "06 78 90 12 34", "07 67 89 01 23" },
                new string[]
                    { "Fontaine", "Isabelle", "isabelle.fontaine@gmail.com", "07 90 12 34 56", "06 78 90 12 34" },
                new string[] { "Rousseau", "David", "david.rousseau@hotmail.com", "08 01 23 45 67", "07 90 12 34 56" },
                new string[] { "Rodriguez", "Anaïs", "anais.rodriguez@yahoo.com", "09 12 34 56 78", "06 01 23 45 67" },
                new string[] { "Müller", "Hans", "hans.muller@gmail.com", "01 23 45 67 89", "07 12 34 56 78" },
                new string[] { "Lefebvre", "Marie", "marie.lefebvre@hotmail.com", "02 34 56 78 90", "06 23 45 67 89" },
                new string[] { "Moreno", "Carlos", "carlos.moreno@yahoo.com", "03 45 67 89 01", "07 34 56 78 90" },
                new string[] { "Benoît", "Sylvie", "sylvie.benoit@gmail.com", "04 56 78 90 12", "06 45 67 89 01" },
                new string[] { "Berger", "Pascal", "pascal.berger@hotmail.com", "05 67 89 01 23", "07 56 78 90 12" },
                new string[] { "Fernández", "José", "jose.fernandez@yahoo.com", "06 78 90 12 34", "06 67 89 01 23" },
                new string[] { "Girard", "Sophie", "sophie.girard@gmail.com", "07 90 12 34 56", "06 67 89 01 23" },
                new string[] {"Rivière", "Antoine", "antoine.riviere@yahoo.com", "09 12 34 56 78", "06 01 23 45 67"},
            new string[] {"Andersen", "Hans", "hans.andersen@gmail.com", "01 23 45 67 89", "07 12 34 56 78"},
            new string[] {"Moulin", "Marie", "marie.moulin@hotmail.com", "02 34 56 78 90", "06 23 45 67 89"},
            new string[] {"Santos", "Pedro", "pedro.santos@yahoo.com", "03 45 67 89 01", "07 34 56 78 90"},
            new string[] {"Lemaire", "Laurent", "laurent.lemaire@gmail.com", "04 56 78 90 12", "06 45 67 89 01"},
            new string[] {"Bouvier", "Julie", "julie.bouvier@hotmail.com", "05 67 89 01 23", "07 56 78 90 12"},
            new string[] {"Pérez", "Antonio", "antonio.perez@yahoo.com", "06 78 90 12 34", "06 67 89 01 23"},
            new string[] {"Renaud", "Céline", "celine.renaud@gmail.com", "07 90 12 34 56", "06 78 90 12 34"},
            new string[] {"González", "Maria", "maria.gonzalez@hotmail.com", "08 01 23 45 67", "07 90 12 34 56"},
            new string[] {"Michel", "François", "francois.michel@yahoo.com", "09 12 34 56 78", "06 01 23 45 67"},
            new string[] {"Leroy", "Marie", "marie.leroy@gmail.com", "01 23 45 67 89", "07 12 34 56 78"},
            new string[] {"Silva", "Ana", "ana.silva@hotmail.com", "02 34 56 78 90", "06 23 45 67 89"},
            new string[] {"Pascal", "Thierry", "thierry.pascal@yahoo.com", "03 45 67 89 01", "07 34 56 78 90"},
            new string[] {"Leclerc", "Claire", "claire.leclerc@gmail.com", "04 56 78 90 12", "06 45 67 89 01"},
            new string[] {"Dumont", "Sylvain", "sylvain.dumont@hotmail.com", "05 67 89 01 23", "07 56 78 90 12"},
            new string[] {"Lopez", "Juan", "juan.lopez@yahoo.com", "06 78 90 12 34", "06 67 89 01 23"},
            new string[] {"Dupuis", "Mireille", "mireille.dupuis@gmail.com", "07 90 12 23 12",  "06 67 89 01 23"},
            new string[] {"Nguyen", "Van", "van.nguyen@hotmail.com", "08 01 23 45 67", "07 90 12 34 56"},
            new string[] {"Rossi", "Mario", "mario.rossi@yahoo.com", "09 12 34 56 78", "06 01 23 45 67"},
            new string[] {"Dubois", "Isabelle", "isabelle.dubois@gmail.com", "01 23 45 67 89", "07 12 34 56 78"},
            new string[] {"Fernández", "Luis", "luis.fernandez@hotmail.com", "02 34 56 78 90", "06 23 45 67 89"},
            new string[] {"Berger", "Alexandre", "alexandre.berger@yahoo.com", "03 45 67 89 01", "07 34 56 78 90"},
            new string[] {"Ruiz", "Antonia", "antonia.ruiz@gmail.com", "04 56 78 90 12", "06 45 67 89 01"},
            new string[] {"Girard", "Jean", "jean.girard@hotmail.com", "05 67 89 01 23", "07 56 78 90 12"},
            new string[] {"Moreau", "Sophie", "sophie.moreau@yahoo.com", "06 78 90 12 34", "06 67 89 01 23"},
            new string[] {"Ali", "Ahmed", "ahmed.ali@gmail.com", "07 90 12 34 56", "07 78 90 12 34"},
            new string[] {"Morales", "Carmen", "carmen.morales@hotmail.com", "08 01 23 45 67", "06 90 12 34 56"},
            new string[] {"Fischer", "Thomas", "thomas.fischer@yahoo.com", "09 12 34 56 78", "07 01 23 45 67"},
            new string[] {"Chevalier", "Aurélie", "aurelie.chevalier@gmail.com", "01 23 45 67 89", "06 12 34 56 78"}
            };
            var rand = new Random();
            foreach (var personne in personnes)
            {
                int service_Id = 1 + rand.Next(6);
                int site_Id = 1 + rand.Next(5);
                Salarie salarie = new Salarie();
                salarie.sit_id = site_Id;
                salarie.ser_id = service_Id;
                salarie.sal_nom = personne[0];
                salarie.sal_prenom = personne[1];
                salarie.sal_mail = personne[2];
                salarie.sal_fixe = personne[3];
                salarie.sal_port = personne[4];
                APIMethods.PostSalarie(salarie);
            }
        }


        private void textBox4_TextChanged(object sender, EventArgs e)
        {
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            if (salId.Text == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de salarié");
                return;
            }
            Salarie salarie = new Salarie();
            salarie.sal_id = Int32.Parse(salId.Text);
            salarie.sit_id = comboBoxFicheSite.SelectedIndex;
            salarie.ser_id = comboBoxFicheService.SelectedIndex;
            salarie.sal_nom = textBox2.Text;
            salarie.sal_prenom = textBox3.Text;
            salarie.sal_mail = textBox6.Text;
            salarie.sal_fixe = textBox4.Text;
            salarie.sal_port = textBox5.Text;
            Debug.WriteLine(salarie.sal_id);
            Debug.WriteLine(salarie.ser_id);
            Debug.WriteLine(salarie.sit_id);
            Debug.WriteLine(salarie.sal_nom);
            Debug.WriteLine(salarie.sal_prenom);
            Debug.WriteLine(salarie.sal_mail);
            Debug.WriteLine(salarie.sal_fixe);
            Debug.WriteLine(salarie.sal_port);
            try
            {
                APIMethods.UpdateSalarie(salarie);
            }
            catch (Exception)
            {
                Debug.WriteLine("exeption");
            }
            fillData();


        }
        private void iconButton3_Click(object sender, EventArgs e)
        {
            pageAdd = new NewSalarie();
            pageAdd.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void siteId_Click(object sender, EventArgs e)
        {
        }

        private void label9_Click(object sender, EventArgs e)
        {
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            if (salId.Text == "")
            {
                MessageBox.Show("Vous n'avez pas selectionner de salarié");
                return;
            }
            int id = Int32.Parse(salId.Text);
            
            APIMethods.DeleteSalarieById(id);
            fillData();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void label10_Click(object sender, EventArgs e)
        {
        }

        private void comboBoxFicheSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            admData = new AdminData();
            admData.Show();
            
        }


        private void button3_Click(object sender, EventArgs e)
        {
            MotDePasse mdp = new MotDePasse();
            mdp.Show();
        }
    }
}
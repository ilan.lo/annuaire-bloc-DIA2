﻿using System;
using System.Windows.Forms;
using Annuaire_Framework.Controllers;
using Annuaire_Framework.Models;

namespace Annuaire_Framework
{
    public partial class MotDePasse : Form
    {
        public MotDePasse()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text != this.textBox2.Text)
            {
                MessageBox.Show("Veuillez entrer le même mot de passe dans les 2 champs");
                return;
            }

            Admin admin = new Admin();
            admin.adm_id = 1;
            admin.mdp = this.textBox1.Text;
            string encodePassword;
            try
            {
                byte[] encData_byte = new byte[this.textBox1.Text.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(this.textBox1.Text);
                encodePassword = Convert.ToBase64String(encData_byte);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue. Veuillez réessayer ulterieurment.");
                return;
            }

            // Vérifie si le mot de passe saisie est valide ou invalide
            admin.mdp = encodePassword;
            APIMethods.UpdateAdmin(admin);
            MessageBox.Show("Vous avez bien changer le mot de passe");
            this.Close();
        }
    }
}
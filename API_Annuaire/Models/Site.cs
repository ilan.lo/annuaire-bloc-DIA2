﻿using Newtonsoft.Json;
namespace API_Annuaire.Models;

public class Site
{
    [JsonProperty("sit_id")]
    public int sit_id { get; set; }
    
    [JsonProperty("sit_ville")]
    public string sit_ville { get; set; }


}
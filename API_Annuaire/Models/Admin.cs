﻿using Newtonsoft.Json;

namespace API_Annuaire.Models;

public class Admin
{
    [JsonProperty("adm_id")]
    public int adm_id { get; set; }
    
    [JsonProperty("mdp")]
    public string mdp { get; set; }
    
}
﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Data;
using API_Annuaire.Models;

namespace API_Annuaire.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ServiceController
{
    [HttpGet]
    public String Get()
    {
        string query = "SELECT * FROM Service";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpGet("{id}")]
    public String GetById(int id)
    {
        string query = "SELECT * FROM service WHERE ser_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpPost("post")]
    public JsonResult Post(Service service)
    {
        string query = @"INSERT INTO service(ser_nom) VALUES(@Nom)";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Nom", service.ser_nom);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Added Successfully");
    }


    [HttpPut("put/{id}")]
    public JsonResult Put(int id, Service service)
    {

        var sql = @"UPDATE service
                    SET ser_nom = @Nom
                    WHERE ser_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(sql, conn);

        cmd.Parameters.AddWithValue("@Nom", service.ser_nom);
        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Updated Successfully");

    }

    [HttpDelete("delete/{id}")]
    public JsonResult Delete(int id)
    {
        string query = @"delete from service where ser_id = @Id;";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Deleted Successfully");
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Data;
using API_Annuaire.Models;

namespace API_Annuaire.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SiteController
{
    [HttpGet]
    public String Get()
    {
        string query = "SELECT * FROM site";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpGet("{id}")]
    public String GetById(int id)
    {
        string query = "SELECT * FROM site WHERE sit_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpPost("post")]
    public JsonResult Post(Site site)
    {
        string query = @"INSERT INTO site(sit_ville) VALUES(@Ville)";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Ville", site.sit_ville);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Added Successfully");
    }


    [HttpPut("put/{id}")]
    public JsonResult Put(int id, Site site)
    {

        var sql = @"UPDATE site
                    SET sit_ville = @Ville
                    WHERE sit_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(sql, conn);

        cmd.Parameters.AddWithValue("@Ville", site.sit_ville);
        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Updated Successfully");

    }

    [HttpDelete("delete/{id}")]
    public JsonResult Delete(int id)
    {
        string query = @"delete from site where sit_id = @Id;";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Deleted Successfully");
    }
}
﻿namespace API_Annuaire.Controllers.DB;
using MySql.Data.MySqlClient;

public class DBConnect
{
    public static MySqlConnection GetDBConnection()
    {
        string host = "127.0.0.1";
        int port = 3306;
        string database = "annuaire_bloc";
        string username = "annuaire";
        string password = "annuaire";

        // Connection String.
        String connString = "Server=" + host + ";Database=" + database
                            + ";port=" + port + ";User Id=" + username + ";password=" + password;

        MySqlConnection conn = new MySqlConnection(connString);

        return conn;
    }
}
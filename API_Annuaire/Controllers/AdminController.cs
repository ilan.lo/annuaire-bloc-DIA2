﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Data;
using API_Annuaire.Models;

namespace API_Annuaire.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AdminController
{
    [HttpGet]
    public String Get()
    {
        string query = "SELECT * FROM admin";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpGet("{id}")]
    public String GetById(int id)
    {
        string query = "SELECT * FROM admin WHERE adm_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpPost("post")]
    public JsonResult Post(Admin admin)
    {
        string query = @"INSERT INTO admin(mdp) VALUES(@Mdp)";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Mdp", admin.mdp);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Added Successfully");
    }


    [HttpPut("put/{id}")]
    public JsonResult Put(int id, Admin admin)
    {

        var sql = @"UPDATE admin
                    SET mdp = @Mdp
                    WHERE adm_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(sql, conn);

        cmd.Parameters.AddWithValue("@Mdp", admin.mdp);
        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Updated Successfully");

    }

    [HttpDelete("delete/{id}")]
    public JsonResult Delete(int id)
    {
        string query = @"delete from admin where adm_id = @Id;";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Deleted Successfully");
    }
}
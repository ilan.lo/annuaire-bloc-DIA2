﻿using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Data;
using API_Annuaire.Models;

namespace API_Annuaire.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SalarieController
{
    [HttpGet]
    public String GetFull()
    {
        string query = @"SELECT * FROM salarie
            natural join service
            natural join site";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpGet("{id}")]
    public String GetFullById(int id)
    {
        string query = @"SELECT * FROM salarie
            natural join service
            natural join site
            WHERE sal_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }
    [HttpGet("like/{search}")]
    public String GetBySearch(string search)
    {
        string query = @"SELECT * FROM salarie
            natural join service
            natural join site
            WHERE sal_prenom LIKE @Search
            OR sal_nom LIKE @Search
            OR sal_fixe LIKE @Search
            OR sal_port LIKE @Search";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);
        search = "%" + search + "%";
        cmd.Parameters.AddWithValue("@Search", search);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        string json = JsonConvert.SerializeObject(table, Formatting.Indented);

        return json;
    }

    [HttpPost("post")]
    public JsonResult Post(Salarie salarie)
    {
        string query = @"INSERT INTO salarie(sal_nom, sal_prenom, sal_fixe, sal_port, sal_mail, ser_id, sit_id)
            VALUES(@Nom, @Prenom, @Fixe, @Port, @Mail, @Service, @Site)";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Nom", salarie.sal_nom);
        cmd.Parameters.AddWithValue("@Prenom", salarie.sal_prenom);
        cmd.Parameters.AddWithValue("@Fixe", salarie.sal_fixe);
        cmd.Parameters.AddWithValue("@Port", salarie.sal_port);
        cmd.Parameters.AddWithValue("@Mail", salarie.sal_mail);
        cmd.Parameters.AddWithValue("@Service", salarie.ser_id);
        cmd.Parameters.AddWithValue("@Site", salarie.sit_id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Added Successfully");
    }


    [HttpPut("put/{id}")]
    public JsonResult Put(int id, Salarie salarie)
    {

        var sql = @"UPDATE salarie
                    SET sal_nom = @Nom,
                        sal_prenom = @Prenom,
                        sal_fixe = @Fixe,
                        sal_port = @Port,
                        sal_mail = @Mail,
                        ser_id = @Service,
                        sit_id = @Site
                    WHERE sal_id = @Id";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(sql, conn);

        cmd.Parameters.AddWithValue("@Nom", salarie.sal_nom);
        cmd.Parameters.AddWithValue("@Prenom", salarie.sal_prenom);
        cmd.Parameters.AddWithValue("@Fixe", salarie.sal_fixe);
        cmd.Parameters.AddWithValue("@Port", salarie.sal_port);
        cmd.Parameters.AddWithValue("@Mail", salarie.sal_mail);
        cmd.Parameters.AddWithValue("@Service", salarie.ser_id);
        cmd.Parameters.AddWithValue("@Site", salarie.sit_id);
        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Updated Successfully");

    }

    [HttpDelete("delete/{id}")]
    public JsonResult Delete(int id)
    {
        string query = @"delete from salarie where sal_id = @Id;";

        DataTable table = new DataTable();
        MySqlDataReader myReader;
        MySqlConnection conn = DB.DBConnect.GetDBConnection();

        conn.Open();
        MySqlCommand cmd = new MySqlCommand(query, conn);

        cmd.Parameters.AddWithValue("@Id", id);

        myReader = cmd.ExecuteReader();
        table.Load(myReader);

        myReader.Close();
        conn.Close();

        return new JsonResult("Deleted Successfully");
    }
}